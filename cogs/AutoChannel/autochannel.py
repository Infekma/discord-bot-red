from redbot.core import commands
from discord import Member
from discord import VoiceState
from discord import VoiceChannel
import discord
import sys
import os

"""
    @commands.command()
    async def autochannel(self, ctx):
        print("Ran Command!")
        # Your code will go here
        await ctx.send("Creating Voice Channel")
        # await create_voice_channel("AutoChannel")

    @commands.Cog.listener()
    async def on_member_join(self, ctx: commands.Context, member):
        channel = member.guild.system_channel
        if channel is not None:
            await channel.send('Welcome {0.mention}.'.format(member))

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, member):
        channel = reaction.message.channel
        if channel is not None:
            await channel.send("{0.mention}, mate, stop fokin' reactin', I'll fokin' shank ya!".format(member))
"""


"""
    @commands.command()
    async def addChannelSpawner(self, ctx):
        print("Adding channel as a spawner")
"""

class AutoChannel(commands.Cog):
    def __init__(self):
        self.spawners = [758831557720014920]
        self.createdChannels = []

    async def delete_voice_channel(self, channel : VoiceChannel):
        if channel is None:
            return

        try:
            await channel.delete()
        except:
            return # failed to delete

    # TODO: store channels
    @commands.command()
    async def clear(self, ctx):
        await ctx.send("Deleting {0} channels".format(len(self.createdChannels)))

        for voiceChannel in self.createdChannels:
            await self.delete_voice_channel(voiceChannel)

        self.createdChannels.clear()
        await ctx.send("[SUCCESS] Clear Voice Channels")

    # if a user left the channel and there are no user left, delete the channel
    async def user_left_channel(self, channel : VoiceChannel):
        if channel not in self.createdChannels:
            return

        if not channel.members:
            await self.delete_voice_channel(channel)

    async def user_joined_spawner(self, member : Member, channel : VoiceChannel):
        if channel.id not in self.spawners:
            return

        # at this point we have confirmed that the channnel is valid & a spawner
        # we will now proceed with creating a new voice channel
        try:
            # clone a new channel and append to list of created spawners and finally move the user
            newVoiceChannel = await channel.clone()
            self.createdChannels.append(newVoiceChannel)
            await member.move_to(newVoiceChannel)
        except:
            return

    @commands.Cog.listener()
    async def on_voice_state_update(self, member : Member, before : VoiceState, after : VoiceState):
        if not before and not after:
            return

        # check if we left a channel that we created from a spawner
        beforeChannel = before.channel
        if beforeChannel is not None:
            await self.user_left_channel(beforeChannel)

        # check if we joined a spawner channel
        afterChannel = after.channel
        if afterChannel is not None:
            await self.user_joined_spawner(member, afterChannel)
